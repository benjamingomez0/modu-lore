import { useState } from "react";

function App() {
     const [count, setCount] = useState(0);
     const handleClick = () => {
          setCount((count) => count + 1);
     };
     return (
          <div className="heading">
               <button onClick={handleClick}>count is {count}</button>
          </div>
     );
}

export default App;
